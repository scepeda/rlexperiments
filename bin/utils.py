import numpy
#from PIL import Image
#from sklearn import preprocessing
import lasagne
import matplotlib.pyplot as plt

def get_mean(x_image,  w,  dim,  ImageShape):
    mean_sample = numpy.zeros((w[0],w[1],dim))
    n_samples = (ImageShape[0]-w[0]//2)*(ImageShape[1]-w[1]//2)
    # slide a window across the image
    for x in range(w[0]//2, ImageShape[0]-w[0]//2, 1):
        for y in range(w[1]//2, ImageShape[1]-w[1]//2, 1):
            window = x_image[(x-w[0]//2):(x+w[0]//2+1), (y-w[1]//2):(y+w[1]//2+1)]
            if window.shape[0] != w[0] or window.shape[1] != w[1]:
                continue
            mean_sample += window.astype(numpy.float_)/n_samples
    return mean_sample

def get_std(x_image,  w,  dim, ImageShape,  mean_sample):
    std_sample = numpy.zeros((w[0],w[1],dim))
    n_samples = (ImageShape[0]-w[0]//2)*(ImageShape[1]-w[1]//2)
    for x in range(w[0]//2, ImageShape[0]-w[0]//2, 1):
        for y in range(w[1]//2, ImageShape[1]-w[1]//2, 1):
            window = x_image[(x-w[0]//2):(x+w[0]//2+1), (y-w[1]//2):(y+w[1]//2+1)]
            if window.shape[0] != w[0] or window.shape[1] != w[1]:
                continue
            sample = (window.astype(numpy.float_) - mean_sample)**2
            std_sample += sample/n_samples
    return numpy.sqrt(std_sample)

def get_predictions(image,  ImageShape, PatternShape, w,  output_model,  x_mean,  x_std):
    #n_batch = 100
    n1 = image.shape[0]
    n2 = image.shape[1]
    diff = (w[0]-1)/2
    valid_windows = n1*n2-diff*2*(n1+n2)+4*diff*diff
    y_preds = numpy.zeros((valid_windows,2))
    c = 0
    # slide a window across the image
    for x in range(w[0]//2, image.shape[0]-w[0]//2, 1):
        for y in range(w[1]//2, image.shape[1]-w[1]//2, 1):
            window = image[(x-w[0]//2):(x+w[0]//2+1), (y-w[1]//2):(y+w[1]//2+1)]
            if window.shape[0] != w[0] or window.shape[1] != w[1]:
                continue
            sample = window.astype(numpy.float_)
            sample -= x_mean
            sample /= x_std
            sample = sample.reshape(1,sample.size)
            y_preds[c] = output_model(sample.astype('float32'))
            c += 1
    return y_preds

def build_custom_mlp(batch_size,  seq_length,  n_features, n_output, input=None, depth=2, width=800, drop_input=.2, drop_hidden=.5,  n_hidden=4):
    #symb_batch_size = batch_size#input.shape[0] # For now, it should be one 
    symb_seq_length = input.shape[1]
    #symb_n_features = n_features#input.shape[2]
    
    network = lasagne.layers.InputLayer(shape=(batch_size,  None, n_features), input_var=input)
    
    # LSTM layer 
    GRAD_CLIP = 10
    #nonlin_rnn = lasagne.nonlinearities.leaky_rectify
    #nonlin_rnn = lasagne.nonlinearities.tanh
    #network = lasagne.layers.LSTMLayer(network, n_hidden, gradient_steps = 20, grad_clipping=GRAD_CLIP, nonlinearity=nonlin_rnn,  only_return_final=False)
    network = lasagne.layers.GRULayer(network, n_hidden, gradient_steps = 100, grad_clipping=GRAD_CLIP,  only_return_final=False)
        
    # Reshape layer, at the output you have a input for the non-recurrent layers of sym_n_features, for each step of each sequence step 
    network = lasagne.layers.ReshapeLayer(network, (batch_size*symb_seq_length, n_hidden))
    
    # Non recurrent layers 
    if drop_input:
        network = lasagne.layers.dropout(network, p=drop_input)
    # Hidden layers and dropout:
    #nonlin = lasagne.nonlinearities.very_leaky_rectify
    nonlin = lasagne.nonlinearities.leaky_rectify
    for _ in range(depth):
        network = lasagne.layers.DenseLayer(network, width, nonlinearity=nonlin)
        if drop_hidden:
            network = lasagne.layers.dropout(network, p=drop_hidden)
    # Output layer:
    last_nonlin = lasagne.nonlinearities.softmax
    #last_nonlin = lasagne.nonlinearities.linear
    network = lasagne.layers.DenseLayer(network, n_output, nonlinearity=last_nonlin)
    
    # Reshape layer
    #network = lasagne.layers.ReshapeLayer(network, (batch_size, symb_seq_length, n_output))
    
    return network

def build_custom_mlp2(batch_size,  seq_length,  n_features, n_output, input=None, depth=2, width=800, drop_input=.2, drop_hidden=.5,  n_hidden=4):
    #symb_batch_size = batch_size#input.shape[0] # For now, it should be one 
    symb_seq_length = input.shape[1]
    #symb_n_features = n_features#input.shape[2]
    
    network = lasagne.layers.InputLayer(shape=(batch_size,  None, n_features), input_var=input)
    
    # Reshape layer, at the output you have a input for the non-recurrent layers of sym_n_features, for each step of each sequence step 
    network = lasagne.layers.ReshapeLayer(network, (batch_size*symb_seq_length, n_features))
    
    # Non recurrent layers 
    if drop_input:
        network = lasagne.layers.dropout(network, p=drop_input)
    # Hidden layers and dropout:
    #nonlin = lasagne.nonlinearities.very_leaky_rectify
    nonlin = lasagne.nonlinearities.leaky_rectify
    for _ in range(depth):
        network = lasagne.layers.DenseLayer(network, width, nonlinearity=nonlin)
        if drop_hidden:
            network = lasagne.layers.dropout(network, p=drop_hidden)
    # Output layer:
    last_nonlin = lasagne.nonlinearities.softmax
    #last_nonlin = lasagne.nonlinearities.linear
    network = lasagne.layers.DenseLayer(network, n_output, nonlinearity=last_nonlin)
    
    # Reshape layer
    network = lasagne.layers.ReshapeLayer(network, (batch_size, symb_seq_length, n_output))
    
    # LSTM layer 
    GRAD_CLIP = 100
    #network = lasagne.layers.LSTMLayer(network, n_hidden, gradient_steps = 40, grad_clipping=GRAD_CLIP, nonlinearity=lasagne.nonlinearities.tanh,  only_return_final=False)
    network = lasagne.layers.GRULayer(network, n_hidden, gradient_steps = 40, grad_clipping=GRAD_CLIP,  only_return_final=False)
    
    # Reshape layer, at the output you have a input for the non-recurrent layers of sym_n_features, for each step of each sequence step 
    network = lasagne.layers.ReshapeLayer(network, (batch_size*symb_seq_length, n_hidden))
    
    last_nonlin = lasagne.nonlinearities.softmax
    #last_nonlin = lasagne.nonlinearities.linear
    network = lasagne.layers.DenseLayer(network, n_output, nonlinearity=last_nonlin)
    
    # Reshape layer
    #network = lasagne.layers.ReshapeLayer(network, (batch_size, symb_seq_length, n_output))
    
    return network
    
def getExperience(env,  experiences,  min_trial_R_idx, output_model,  trial,  render_freq,  n_features,  n_action,  n_trials,  debug_level):
    observation = env.reset()
    episode = []
    sum_R = 0
    for step in range(10**6):
        if(step >= 1 and (trial % render_freq) == 0):
            env.render()
        # FeedForward
        #observations = numpy.zeros((1, episode_length, n_features))
        episode_length = (step + 1)
        if(step == 0):
            observations = observation.reshape(1, 1, n_features)
        else:
            observations = numpy.concatenate((observations,  observation.reshape(1, 1, n_features)), axis=1)
        policy = output_model(observations.reshape(1, episode_length, n_features).astype('float32'))
        last_policy = policy[episode_length - 1]
        if(debug_level >= 3):
            print('observations.shape: {}'.format(observations.shape))
            print('policy.shape: {}'.format(policy.shape))
            print('policy: {}'.format(policy))
            print('last_policy: {}'.format(last_policy))
        action = numpy.random.choice(n_action, 1, p=last_policy)[0]
        next_observation, reward, bDone, info = env.step(action)
        sum_R += reward
        # FeedBack
        action_aux = numpy.zeros(1) + action
        episode.append(observation.flatten())
        episode.append(reward)
        episode.append(action_aux)
        observation = next_observation
        if (sum_R < -400 or bDone):
            #print("Done!")
            break
    if(len(experiences) < n_trials):
        experiences.append(episode)
    else:
        #experiences[trial%n_trials] = episode
        experiences[min_trial_R_idx] = episode

def plot(x, y,  name):
    fig, ax = plt.subplots(nrows=1, ncols=1)
    ax.plot(x, y, 'r-')
    fig.savefig(name)
    plt.close(fig)
